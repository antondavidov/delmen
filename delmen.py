from .. import loader, utils

class DelmeMod(loader.Module):
    """Removing all messages"""
    strings = {'name': 'DelMe'}
    @loader.sudo
    async def delmecmd(self, message):
        """Removing all your messages"""
        chat = message.chat
        if chat:
            args = utils.get_args_raw(message)
            if args != str(message.chat.id+message.sender_id):
                await message.edit(f"<b>If you really want to do this, write:</b>\n<code>.delme {message.chat.id+message.sender_id}</code>")
                return
            await delete(chat, message, True)
        else:
            await message.edit("<b>I don't remove private messages!</b>")
    @loader.sudo
    async def delmenowcmd(self, message):
        """Removes all messages from you without questions"""
        chat = message.chat
        if chat:
            await delete(chat, message, False)
        else:
            await message.edit("<b>I don't remove private messages!</b>")

async def delete(chat, message, now):
    if now:
        all = (await message.client.get_messages(chat, from_user="me")).total
        await message.edit(f"<b>{all} messages will be deleted!</b>")
    else: await message.delete()
    _ = not now
    async for msg in message.client.iter_messages(chat, from_user="me"):
        if _:
            await msg.delete()
        else:
            _ = "_"
    await message.delete() if now else "Fuck the police Fuck the police Fuck the police Fuck the police"
